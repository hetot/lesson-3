// Lesson 3
function fizzBuzz () {
    for (let i = 1; i <= 100; i++) {
        let ans = ''
        if (i % 3 == 0) {
            ans = ans + 'Fizz'
        }
        if (i % 5 == 0) {
            ans = ans + 'Buzz'
        }
        if (ans == '') {
            ans = String(i)
        }
        console.log(ans)
    }
}

function filterArray (list) {
    let my_ans = new Array()
    for (let i = 0; i < list.length; i++) {
        if (Array.isArray(list[i])) {
            for (let j = 0; j < list[i].length; j++) {
                if (!my_ans.includes(list[i][j])) {
                    my_ans.push(list[i][j])
                }
            }
        }
    }
    return my_ans.sort(function(a, b){return a - b})
}
// const fruits = ['apple', 'banana']

// const vegetables = [ ...fruits ] // 0 index
    // console.log(vegetables)
// Output DON'T TOUCH!
// console.log(fizzBuzz())
const letList = [
    [2], 
    23,
    'dance',
    [7,8,9, 7],
    true, 
    [3, 5, 3],
    [777,855, 9677, 457],
]
console.log(filterArray(letList)) // 2,3,5,7,8,9